#/bin/bash

R="R"

cd $(dirname $0)

# Check everything is ok
# $R CMD check ./bfca

# source
$R CMD build ./bfca

# mac binary
$R CMD INSTALL --build ./bfca

# Windows
# Copy bfca folder (code, not the one including the bfca tools) to desktop and:
# cd Desktop
# "C:\Program Files\R\R-3.0.2\bin\x64\R.exe" CMD INSTALL --build bfca

# Clean
$R CMD INSTALL --clean ./bfca
