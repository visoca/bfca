bfc<-
function(x, method="smoothed", th="1", bootstrap=0, cores=1)
{	
	x.list <- list()
	for (i in 1:ncol(x)) x.list[[i]] <- as.numeric(x[,i])

	
	cal.labels <- levels(as.factor(sapply(
						strsplit(labels(x[1,]),"[_|.]",perl=TRUE),
						function (x) c(x[length(x)],x[length(x)-1])
						)))

	log.labels <- levels(as.factor(sapply(
						strsplit(labels(x[1,]),"[_|.]",perl=TRUE),
						function (x) paste(x[length(x)-1],x[length(x)],sep=".")
						)))
	
	disim.lnPdata <- function(x,maximum){return(abs(x-maximum))}

	if (cores > 1) sfInit(parallel=TRUE, cpus=cores, type="SOCK")
	else sfInit(parallel=FALSE)
		
	if (method == 'harmonic') lnPdata <- as.vector(sfLapply(x.list, harmonic.estimator))
	if (method == 'smoothed') lnPdata <- as.vector(sfLapply(x.list, smoothed.estimator))	
	
	if (bootstrap > 1) {
		if (method == 'harmonic') StdErr <- as.vector(lapply(x.list, SE.estimator, nbootstrap=bootstrap, method="harmonic"))
		if (method == 'smoothed') StdErr <- as.vector(lapply(x.list, SE.estimator, nbootstrap=bootstrap, method="smoothed"))
	}

	sfStop()
	
	ncals <- ((sqrt(1+4*2*length(lnPdata))+1)/2)	
	
	max.lnPdata <- max(unlist(lnPdata))
	lnBF <- sapply(as.vector(lnPdata), disim.lnPdata, maximum=max.lnPdata)

	lnPdata.frame <- as.data.frame(do.matrix(lnPdata), row.names=cal.labels)
	names(lnPdata.frame) <- cal.labels

	if (bootstrap > 1) {
		StdErr.frame <- as.data.frame(do.matrix(StdErr), row.names=cal.labels)
		names(StdErr.frame) <- cal.labels
	}
	
	lnBF.frame <- as.data.frame(do.matrix(lnBF), row.names=cal.labels)
	names(lnBF.frame) <- cal.labels
	
	lnBF.2by2 <- data.frame(lnBF, row.names=log.labels)
	
	c <- 1
	clusters.labels <- character()
	clusters.size <- numeric()
	clusters.max <- numeric()
	for (n in 2:ncals){
		cmbs <- combn(cal.labels,n)
		for (i in 1:length(cmbs[1,])){
			cmbs2by2 <- combn(cmbs[,i],2)
			gr <- apply(cmbs2by2, 2, paste, collapse=".")
			max.lnBF <- max(lnBF.2by2[gr, ])			
			clusters.labels[c] <- paste(cmbs[ ,i], collapse=".")
			clusters.size[c] <- n
			clusters.max[c] <- max.lnBF
			c <- c + 1
		}
	}	
	
	clusters.frame <- data.frame(clusters.labels, clusters.size, clusters.max)
	names(clusters.frame) <- c("cluster", "size", "max.lnBF")
	clusters.frame <- clusters.frame[with(clusters.frame, order(-size,max.lnBF)), ]

	congruent.clusters.frame <- subset(clusters.frame, max.lnBF < th)
	
	if (bootstrap > 1) list(lnPdata=lnPdata.frame, StdErr=StdErr.frame, lnBF=lnBF.frame, clusters.lnBF=clusters.frame, congruent.clusters.lnBF=congruent.clusters.frame)
	else list(lnPdata=lnPdata.frame, lnBF=lnBF.frame, clusters.lnBF=clusters.frame, congruent.clusters.lnBF=congruent.clusters.frame)
}
