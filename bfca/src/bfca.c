#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
#include <R.h>

// Auxiliary functions
// ------------------------------------------------------------------------
double logsumtrick (double value1, double value2){
	int NATS = 400;
	double loglimit = -3.40282347E+36;
	double resta = value2 - value1;
	double out;
	if (resta > NATS || value1 < loglimit){
		out = value2;
	}
	else{
		if (resta < -NATS || value2 < loglimit){
			out = value1;
		}
		else{
			if (resta < 0){
				out = value1 + log1p(exp(resta));
			}
			else{
				out = value2 + log1p(exp(-resta));
			}
		}
	}
	return out;
}

double smestcmp(double *array, int size, double delta, double pdata){
	double logdelta = log(delta);
	double loginvdelta = log(1 - delta);
	double loglen = log(size);
	double offset = loginvdelta - pdata;
	double bottom = loglen + logdelta - loginvdelta;
	double top = bottom + pdata;
	double weight;
	
	int i;
	for(i = 0; i < size; i++){
		weight = -logsumtrick(logdelta, offset + array[i]);
		top = logsumtrick(top, weight + array[i]);
		bottom = logsumtrick(bottom, weight);
	}
	double out = top - bottom;
	
	return out;
}
// ------------------------------------------------------------------------	


// ------------------------------------------------------------------------	
// ---------  POSTERIOR HARMONIC ESTIMATOR (HARMONIC ESTIMATOR)  ----------	
// ------------------------------------------------------------------------	

void harmest(double *data, int *datasize, double *p){

	int size;
	double *array = malloc(1000000*sizeof(double));
	double pdata;
	
	size = *datasize;

	double suma = 0;
	int i;
	for(i = 0; i < size; i++){
		suma += data[i];
	}

	double denom = -3.40282347E+36;
	for(i = 0; i < size; i++){
		denom = logsumtrick(denom, suma - data[i]);
	}

	pdata = suma - denom + log(size);
	*p=pdata;
	return;
}
// ------------------------------------------------------------------------	

// ------------------------------------------------------------------------	
// ------  STABILIZED HARMONIC MEAN ESTIMATOR (SMOOTHED ESTIMATOR)  -------
// ------------------------------------------------------------------------	
void smoothest(double *data, int *datasize, double *p){

	int size;
	double *array = malloc(10000*sizeof(double));
	double pdata;
	
	size = *datasize;

	// Posterior harmonic estimator
	// ......................................................
	double suma = 0;
	int i;
	for(i = 0; i < size; i++){
		suma += data[i];
	}
	
	double denom = -3.40282347E+36;
	for(i = 0; i < size; i++){
		denom = logsumtrick(denom, suma - data[i]);
	}
	
	pdata = suma - denom + log(size);
	// ......................................................

	double delta = 0.01;
	double deltaP = 1;
	double tolerance = 0.001;
	int maxiter = 400;
	int iter = 0;
	double g1, pdata2, dx, g2, dgdx, pdata3, g3;
	
	while(fabs(deltaP) > tolerance && iter < maxiter){
		g1 = smestcmp(data, size, delta, pdata) - pdata;
		pdata2 = pdata + g1;
		dx = g1 * 10;
		g2 = smestcmp(data, size, delta, (pdata + dx)) - (pdata + dx);
		dgdx = (g2 - g1) / dx;
		pdata3 = pdata - g1 / dgdx;
		
		if (pdata3 < 2 * pdata || pdata3 > 0 || pdata3 > 0.5 * pdata){
			pdata3 = pdata + 10 * g1;				
		}
		
		g3 = smestcmp(data, size, delta, pdata3) - pdata3;
		
		if (abs(g3) <= abs(g2) && (g3 > 0 || abs(dgdx) > 0.01)){
			deltaP = pdata3 - pdata;
			pdata = pdata3;
		}
		else{
			if (abs(g2) <= abs(g1)){
				pdata2 = pdata2 + g2;
				deltaP = pdata2 - pdata;
				pdata = pdata2;					
			}
			else{
				deltaP = g1;
				pdata = pdata + g1;
			}
		}
		iter++;	
		double check = abs(deltaP);
	}
	*p = pdata;
}
// ------------------------------------------------------------------------	
