#!/usr/bin/perl

# Copyright (C) 2012 Victor Soria-Carrasco
# Department of Animal and Plant Sciences
# University of Sheffield
# Sheffield S10 2TN
# United Kingdom
# v.soria-carrasco@sheffield.ac.uk

# Previous address:
# Institute of Evolutionary Biology (IBE), CSIC-UPF
# Pg. Maritim de la Barceloneta 37-42, 08003 Barcelona, Spain
# victor.soria@ibe.upf-csic.es

# This program is part of the bfca tools

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Requirements:
#	Perl v.5.8.x or greater (previous versions have not been tested)

use warnings;
use strict;


# Given an XML BEAST file and a table with calibration points, return an xml file ready to be run with BEAST
#
#	bfca_tool3_final.pl -i <xml file> -c <calibrations/constraints file> -o <output file>

# CHECK POSIX
# CHECK STDERR AND EXIT WITH NUMBERS ??
# CHECK INPUT TEXT
# CHECK SO (path slashes)

# Input as -i (--input) -c (--constraints) -o (--output)
# Check number of arguments: 
#	if 3 take it in order (input,constraints,output);
#	if 6 take it following flags
#	else show usage


usage() if (scalar(@ARGV) < 4);

my ($xml, $constraints, $outfile, $basename) = get_cmd_line(@ARGV);

usage() if (!defined ($xml) || !defined($constraints));

author();

# Read xml input file
open (FILE, "$xml") or die ("\n\tCannot open $xml\n");
	my @beast_xml=(<FILE>);
close (FILE);

# Read calibrations/constraints file
my @constraints=();
open (FILE, "$constraints") or die ("\n\tCannot open $constraints\n");
	my $st=0;
	while (<FILE>){
		if ($st == 0){ $st=1; next;}
		next if (/^[\n|\#]/);
		chomp($_);
		my @aux=split(/\t/,$_);
		push (@constraints, [@aux]);
	}
close (FILE);

# Add clock estimation (if needed)
@beast_xml=clock_estimation(@beast_xml);

# Generate xml file with all calibrations/constraints
for (my $i=0; $i < scalar(@constraints); $i++){
	my $taxablock=get_taxablock(@constraints);
	my $treeblock=get_treeblock(@constraints);
	my $tmrcastatblock=get_tmrcastatblock(@constraints);
	my $priorblock=get_priorblock(@constraints);
	my $logblock=get_logblock(@constraints);
	open (FILE, ">$outfile") or die ("\n\tCannot open $outfile\n");
	my $stop=0;
	my $xml_main_block="taxa";
	foreach my $b (@beast_xml){
		# Where am I?
		if ($b=~ m/^\t\<[a-z]/){
			$xml_main_block=$b;
			chomp($xml_main_block);
			$xml_main_block=~ s/\t\<//g;
			$xml_main_block=~ s/\ .+//g;
			$xml_main_block=~ s/>$//g;
		}
		#Insert taxa block
		if ($xml_main_block eq "taxa" && 
			$b=~ m/^\t\<\!\-\-\ The\ sequence\ alignment/){
			print FILE $taxablock;
		}
		#Insert tree block
		elsif ($b=~ m/^\t\<\!\-\-\ Generate\ a\ random\ starting\ tree/){
			print FILE $treeblock;
			$stop=1;
		}
		#Insert tmrca stats block
		elsif ($b=~ m/^\t\<\!\-\-\ Define\ operators/){
			print FILE $tmrcastatblock;
		}
		#Insert prior block
		elsif (	$xml_main_block eq "mcmc" && 
				$b=~ m/^\t\t\t\<prior\ id\=\"prior\"\>/){
			print FILE $priorblock;
			next;
		}
		elsif (	$xml_main_block eq "mcmc" && 
				$b=~ m/^\t\t\t\<parameter\ idref\=\"yule\.birthRate\"/){
			print FILE $logblock;
		}
		elsif ( $b=~ m/fileName/){
			if ($b=~ m/\(/){
				$b=~ s/fileName\=\".+\.\(/fileName\=\"$basename\.\(/g;
			}
			else{
				$b=~ s/fileName\=\".+\./fileName\=\"$basename\./g;
			}
		}

		$stop=0 if ($stop==1 && 
			($b=~ m/^\t\<\!\-\-\ Generate\ a\ tree\ model/));

		print FILE $b if ($stop == 0);
	}
	close (FILE);
}

print "\n\tXML file have been saved as $outfile\n\n";


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------- SUBROUTINES ----------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

sub author{
	my $auth=<<BLOCK;

	Copyright (C) 2012 Victor Soria-Carrasco
	Department of Animal and Plant Sciences
	University of Sheffield
	Sheffield S10 2ST
	United Kingdom
	v.soria-carrasco\@sheffield.ac.uk

BLOCK
	print "$auth";
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub usage{
	author();
	print "\tbfca_tool3_final.pl -i <xml file> -c <calibrations/constraints file> -o <output file>\n\n";
	exit();
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_cmd_line{
	my @cmd=(@_);
	my $xml;
	my $constraints;
	my $outfile;
	my $basename;

	my $op;
	foreach (@ARGV){
		if (/-i/){ $op="xml"; next; }
		elsif (/-c/){ $op="constraints"; next; }
		elsif (/-o/){ $op="outfile"; next; }

		if ($op eq "xml"){ $xml = $_; }
		elsif($op eq "constraints"){ $constraints = $_; }
		elsif($op eq "outfile"){ $outfile = $_; }
	}
	if (defined ($outfile)){
		$basename=$outfile;
		$basename=~ s/.+[\/|\\]//g;
		$basename=~ s/\..+//g;
	}

	return ($xml, $constraints, $outfile, $basename);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_taxablock{
	# DEFINE TAXA BLOCK
	my @calibrations=@_;

	my $taxablock="";
	foreach my $cal (@calibrations){
		$taxablock.='	<taxa id="'.${$cal}[0].'">'."\n";
		my @taxa=split(/\,/,${$cal}[1]);
		foreach my $t (@taxa){
			$taxablock.='		<taxon idref="'.$t.'"/>'."\n";
		}
		$taxablock.='	</taxa>'."\n";
	}
	return($taxablock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub clock_estimation{
	my @xml=@_;

	my @cl=();
	foreach (@xml){
		if (/parameter.+clock\.rat.+value/ || /parameter.+ucld\.mean.+value/ || /parameter.+uced\.mean.+value/){				
			my $aux=$_;
			chomp($aux);
			$aux=~ s/.+id\=\"//g;
			$aux=~ s/\".+//g;
			push (@cl, $aux) if (!grep(/^$aux$/, @cl));
		}
	}

	my @newxml=();
	my $jump=0;

	foreach (@xml){
		if ($jump > 0){
			$jump--;
			next;
		}
		
		# Change prior
		if (/\<parameter id\=\".*clock\.rate\"/){
			s/clock\.rate.+/clock\.rate\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		if (/\<parameter id\=\".*ucld\.mean\"/){
			s/ucld\.mean.+/ucld\.mean\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		if (/\<parameter id\=\".*uced\.mean\"/){
			s/uced\.mean.+/uced\.mean\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		
		push (@newxml, $_);
		
		# Add scaleOperator
		
		if (/\<operators id\=/){
			foreach my $c (@cl){
				push (@newxml,
					"\t\t".'<scaleOperator scaleFactor="0.75" weight="3">'."\n");
				push (@newxml,
					"\t\t\t".'<parameter idref="'.$c.'"/>'."\n");
				push (@newxml,
					"\t\t".'</scaleOperator>'."\n");
			}
		}

		# Add upDownOperator
		if (/<up>/){
			my $c=shift(@cl);
			push (@newxml,
				"\t\t\t\t".'<parameter idref="'.$c.'"/>'."\n");
		}
	}
	
	return(@newxml);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_tmrcastatblock{
	#DEFINE TMRCA STATS BLOCK
	my @calibrations=@_;

	my $tmrcastatblock="";
	foreach my $cal (@calibrations){
		$tmrcastatblock.="\t".'<tmrcaStatistic id="tmrca('.${$cal}[0].')">'."\n";
		$tmrcastatblock.="\t\t".'<mrca>'."\n";
		$tmrcastatblock.="\t\t\t".'<taxa idref="'.${$cal}[0].'"/>'."\n";
		$tmrcastatblock.="\t\t".'</mrca>'."\n";
		$tmrcastatblock.="\t\t".'<treeModel idref="treeModel"/>'."\n";
		$tmrcastatblock.="\t".'</tmrcaStatistic>'."\n";
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$tmrcastatblock.="\t".'<monophylyStatistic id="monophyly('.${$cal}[0].')">'."\n";
			$tmrcastatblock.="\t\t".'<mrca>'."\n";
			$tmrcastatblock.="\t\t\t".'<taxa idref="'.${$cal}[0].'"/>'."\n";
			$tmrcastatblock.="\t\t".'</mrca>'."\n";
			$tmrcastatblock.="\t\t".'<treeModel idref="treeModel"/>'."\n";
			$tmrcastatblock.="\t".'</monophylyStatistic>'."\n";
		}
	}

	return ($tmrcastatblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_treeblock{
	# DEFINE TREE BLOCK
	my @calibrations=@_;

	my $treeblock='	<!-- Generate a random starting tree under the coalescent process            -->'."\n";
	$treeblock.='	<!-- Has calibration                                                         -->'."\n";
	$treeblock.='	<coalescentTree id="startingTree">'."\n";
	$treeblock.='		<constrainedTaxa>'."\n";
	$treeblock.='			<taxa idref="taxa"/>'."\n";

	foreach my $cal (@calibrations){
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$treeblock.='			<tmrca monophyletic="true">'."\n";
		}
		else{
			$treeblock.='			<tmrca monophyletic="false">'."\n";
		}
		$treeblock.='				<taxa idref="'.${$cal}[0].'"/>'."\n";
		if (${$cal}[3] eq "truncated-normal"){
			# Distribution block
			$treeblock.='				<uniformDistributionModel>'."\n";
			$treeblock.='					<lower>'.${$cal}[6].'</lower>'."\n";
			$treeblock.='					<upper>'.${$cal}[7].'</upper>'."\n";
			$treeblock.='				</uniformDistributionModel>'."\n";
		}
		elsif(${$cal}[3] eq "uniform"){
			# Distribution block
			$treeblock.='				<uniformDistributionModel>'."\n";
			$treeblock.='					<lower>'.${$cal}[4].'</lower>'."\n";
			$treeblock.='					<upper>'.${$cal}[5].'</upper>'."\n";
			$treeblock.='				</uniformDistributionModel>'."\n";
		}
		$treeblock.='			</tmrca>'."\n";
	}

	$treeblock.='		</constrainedTaxa>'."\n";
	$treeblock.='		<constantSize idref="initialDemo"/>'."\n";
	$treeblock.='	</coalescentTree>'."\n";

	return($treeblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_priorblock{
	#DEFINE PRIOR BLOCK
	my @calibrations=@_;

	my $priorblock="\t\t\t".'<prior id="prior">'."\n";
	$st=0;
	my $monophylies="\t\t\t\t".'<booleanLikelihood>'."\n";
	foreach my $cal (@calibrations){
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$monophylies.="\t\t\t\t\t".'<monophylyStatistic idref="monophyly('.${$cal}[0].')"/>'."\n";
			$st=1;
		}
	}
	$monophylies.="\t\t\t\t".'</booleanLikelihood>'."\n";
	$priorblock.=$monophylies if ($st==1);

	foreach my $cal (@calibrations){
		if (${$cal}[3] eq "lognormal"){
			$priorblock.="\t\t\t\t".'<logNormalPrior mean="'.${$cal}[4].'" stdev="'.${$cal}[5].'" offset="'.${$cal}[6].'" meanInRealSpace="true">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</logNormalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "normal"){
			$priorblock.="\t\t\t\t".'<normalPrior mean="'.${$cal}[4].'" stdev="'.${$cal}[5].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</normalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "exponential"){
			$priorblock.="\t\t\t\t".'<exponentialPrior mean="'.${$cal}[4].'" offset="'.${$cal}[5].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</exponentialPrior>'."\n";
		}
		elsif (${$cal}[3] eq "truncated-normal"){
			$priorblock.="\t\t\t\t".'<uniformPrior lower="'.${$cal}[6].'" upper="'.${$cal}[7].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</uniformPrior>'."\n";
			$priorblock.="\t\t\t\t".'<normalPrior mean="'.${$cal}[4].'" stdev="'.${$cal}[5].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</normalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "gamma"){
			$priorblock.="\t\t\t\t".'<gammaPrior shape="'.${$cal}[4].'" scale="'.${$cal}[5].'" offset="'.${$cal}[6].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</gammaPrior>'."\n";
		}
		elsif (${$cal}[3] eq "invgamma"){
			$priorblock.="\t\t\t\t".'<invgammaPrior shape="'.${$cal}[4].'" scale="'.${$cal}[5].'" offset="'.${$cal}[6].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</invgammaPrior>'."\n";
		}
		elsif (${$cal}[3] eq "uniform"){
			$priorblock.="\t\t\t\t".'<uniformPrior lower="'.${$cal}[4].'" upper="'.${$cal}[5].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</uniformPrior>'."\n";
		}
		elsif (${$cal}[3] eq "laplace"){
			$priorblock.="\t\t\t\t".'<laplacePrior mean="'.${$cal}[4].'" scale="'.${$cal}[5].'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</laplacePrior>'."\n";
		}
	}

	return($priorblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_logblock{
	# DEFINE LOG BLOCK
	my @calibrations=@_;

	my $logblock="";
	foreach my $cal (@calibrations){
		$logblock.="\t\t\t".'<tmrcaStatistic idref="tmrca('.${$cal}[0].')"/>'."\n";
	}

	return($logblock);
}
