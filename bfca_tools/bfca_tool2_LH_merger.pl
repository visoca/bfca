#!/usr/bin/perl

# Copyright (C) 2012 Victor Soria-Carrasco
# Department of Animal and Plant Sciences
# University of Sheffield
# Sheffield S10 2TN
# United Kingdom
# v.soria-carrasco@sheffield.ac.uk

# Previous address:
# Institute of Evolutionary Biology (IBE), CSIC-UPF
# Pg. Maritim de la Barceloneta 37-42, 08003 Barcelona, Spain
# victor.soria@ibe.upf-csic.es

# This program is part of the bfca tools

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Requirements:
#	Perl v.5.8.x or greater (previous versions have not been tested)

# Changelog
# 1.0 May  2011 - First release - BEAST
# 1.01 June 2012 - Slight changes - author address/email

use warnings;
use strict;

# Given a folder with several runs of BEAST, extract likelihoods and creates
# a new table ready to work with BFCA R package


#	bfca_tool2_LH_merger.pl -i <folder with BEAST logs> -o <output file>


usage() if (scalar(@ARGV) < 4);

my ($indir, $outfile) = get_cmd_line(@ARGV);

usage() if (!defined ($indir) || !defined($outfile));

author();


opendir(DIR, "$indir");
	my @BEASTlogs=grep (/\.log$/i, readdir(DIR));
closedir (DIR);

die ("\n\tERROR: There are not log files in the folder $ARGV[0]\n\n") if (scalar(@BEASTlogs) == 0);

print "\n";
my @LIKELIHOODS=();
foreach my $bl (@BEASTlogs){
	my @lh=();
	print "\tReading file $indir/$bl...\n";
	open (FILE, "$indir/$bl");
		while (<FILE>){
			if (/^[0-9]/){
				my @aux=split(/\t/,$_);
				push (@lh, $aux[3]);
			}
		}
		push (@LIKELIHOODS, [@lh]);
	close (FILE);
}

print "\n";

print "\tGenerating likelihood table...\n\n";
open (FILE, ">$outfile");
	for (my $i=0; $i < scalar($#BEASTlogs); $i++){
		$BEASTlogs[$i]=~ s/\.log//g;
		$BEASTlogs[$i]="LH_".$BEASTlogs[$i];
		print FILE "$BEASTlogs[$i]\t";
	}
	$BEASTlogs[$#BEASTlogs]=~ s/\.log//g;
	$BEASTlogs[$#BEASTlogs]="LH_".$BEASTlogs[$#BEASTlogs];
	print FILE "$BEASTlogs[$#BEASTlogs]\n";

	for (my $i=0; $i < scalar(@{$LIKELIHOODS[1]}); $i++){
		my $line="";
		for (my $j=0; $j < ($#LIKELIHOODS); $j++){
			print FILE "$LIKELIHOODS[$j][$i]\t";
		}
		print FILE "$LIKELIHOODS[$#LIKELIHOODS][$i]\n";
	}
close (FILE);

print "\tOutput saved in $outfile\n\n";


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------- SUBROUTINES ----------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

sub author{
	my $auth=<<BLOCK;

	Copyright (C) 2012 Victor Soria-Carrasco
	Department of Animal and Plant Sciences
	University of Sheffield
	Sheffield S10 2ST
	United Kingdom
	v.soria-carrasco\@sheffield.ac.uk

BLOCK
	print "$auth";
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub usage{
	author();
	print "\tbfca_tool2_LH_merger.pl -i <folder with BEAST logs> -o <output file>\n\n";
	exit();
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_cmd_line{
	my @cmd=(@_);
	my $indir;
	my $outfile;

	my $op;
	foreach (@ARGV){
		if (/-i/){ $op="indir"; next; }
		elsif (/-o/){ $op="outfile"; next; }

		if ($op eq "indir"){ $indir = $_; }
		elsif($op eq "outfile"){ $outfile = $_; }
	}
	return ($indir, $outfile);
}
