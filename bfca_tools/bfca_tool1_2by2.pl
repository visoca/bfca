#!/usr/bin/perl

# Copyright (C) 2012 Victor Soria-Carrasco
# Department of Animal and Plant Sciences
# University of Sheffield
# Sheffield S10 2TN
# United Kingdom
# v.soria-carrasco@sheffield.ac.uk

# Previous address:
# Institute of Evolutionary Biology (IBE), CSIC-UPF
# Pg. Maritim de la Barceloneta 37-42, 08003 Barcelona, Spain
# victor.soria@ibe.upf-csic.es

# This program is part of the bfca tools

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

# Requirements:
#	Perl v.5.8.x or greater (previous versions have not been tested)

# Changelog
# 1.0 May  2011 - First release - support for BEAST 1.5.x
# 1.01 June 2012 - Slight changes - author address/email


use warnings;
use strict;

# Given an XML BEAST file and a table with calibration points, return xml files ready to be run
# for a BFC analysis
#
#	bfca_tool1_2by2.pl -i <xml file> -c <calibrations/constraints file> -o <output directory>

# CHECK POSIX
# CHECK STDERR AND EXIT WITH NUMBERS ??
# CHECK INPUT TEXT
# CHECK SO (path slashes)


# Check number of arguments: 
#	if 3 take it in order (input,constraints/calibrations,output);
#	if 6 take it following flags
#	else show usage


usage() if (scalar(@ARGV) < 4);

my ($xml, $constraints, $outdir, $basename) = get_cmd_line(@ARGV);

usage() if (!defined ($xml) || !defined($constraints));

author();

# Read xml input file
open (FILE, "$xml") or die ("\n\tCannot open $xml\n");
	my @beast_xml=(<FILE>);
close (FILE);

# Read calibrations/constraints file
my @constraints=();
my @calibrations=();
open (FILE, "$constraints") or die ("\n\tCannot open $constraints\n");
	my $st=0;
	while (<FILE>){
		if ($st == 0){ $st=1; next;}
		next if (/^[\n|\#]/);
		chomp($_);
		my @aux=split(/\t/,$_);
		if ($aux[3] eq "NA"){
			push (@constraints, [@aux]);
		}
		else{
			push (@calibrations, [@aux]);
		}
	}
close (FILE);

# Create output dir
mkdir "$outdir";
print "\n";

# Add clock estimation (if needed)
@beast_xml=clock_estimation(@beast_xml);

# Generate BFC 2by2 xml files
print "\tCombinations generated:\n\n";
for (my $i=0; $i < scalar(@calibrations); $i++){
	for (my $j=($i+1); $j < scalar(@calibrations); $j++){
		print "\t\t$calibrations[$i][0] - $calibrations[$j][0]\n";
		my @pair=($calibrations[$i],$calibrations[$j]);
		my $outfile=$basename."\.".$calibrations[$i][0]."\.".$calibrations[$j][0].'.xml';

		my @constraintsandpair=(@constraints,@pair);

		my $taxablock=get_taxablock(@constraintsandpair);
		my $treeblock=get_treeblock(@constraintsandpair);
		my $tmrcastatblock=get_tmrcastatblock(@constraintsandpair);
		my $priorblock=get_priorblock(@constraintsandpair);
		my $logblock=get_logblock(@constraintsandpair);

		open (FILE, ">$outdir/$outfile") or die ("\n\tCannot open $outfile\n");
		my $stop=0;
		my $xml_main_block="taxa";
		foreach my $b (@beast_xml){
			# Where am I?
			if ($b=~ m/^\t\<[a-z]/){
				$xml_main_block=$b;
				chomp($xml_main_block);
				$xml_main_block=~ s/\t\<//g;
				$xml_main_block=~ s/\ .+//g;
				$xml_main_block=~ s/>$//g;
			}
			#Insert taxa block
			if ($xml_main_block eq "taxa" && 
				$b=~ m/^\t\<\!\-\-\ The\ sequence\ alignment/){
				print FILE $taxablock;
			}
			#Insert tree block
			elsif ($b=~ m/^\t\<\!\-\-\ Generate\ a\ random\ starting\ tree/){
				print FILE $treeblock;
				$stop=1;
			}
			#Insert tmrca stats block
			elsif ($b=~ m/^\t\<\!\-\-\ Define\ operators/){
				print FILE $tmrcastatblock;
			}
			#Insert prior block
			elsif (	$xml_main_block eq "mcmc" && 
					$b=~ m/^\t\t\t\<prior\ id\=\"prior\"\>/){
				print FILE $priorblock;
				next;
			}
			elsif (	$xml_main_block eq "mcmc" && 
					$b=~ m/^\t\t\t\<parameter\ idref\=\"yule\.birthRate\"/){
				print FILE $logblock;
			}
			elsif ( $b=~ m/fileName/){
				if ($b=~ m/\(/){
					$b=~ s/fileName\=\".+\.\(/fileName\=\"$basename\.$calibrations[$i][0]\.$calibrations[$j][0]\.\(/g;
				}
				else{
					$b=~ s/fileName\=\".+\./fileName\=\"$basename\.$calibrations[$i][0]\.$calibrations[$j][0]\./g;
				}
			}
			# Do not generate trees, only log
			if ($b=~ m/write tree log to file/){
				$stop=1;
			}

			$stop=0 if ($stop==1 && 
				(($b=~ m/^\t\<\!\-\-\ Generate\ a\ tree\ model/) ||
				($b=~ m/\/mcmc/)));

			print FILE $b if ($stop == 0);
		}
		close (FILE);
	}
}

print "\n\tXML files for BFC analysis have been saved in $outdir\n\n";


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
#-------------------------------- SUBROUTINES ----------------------------------
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

sub author{
	my $auth=<<BLOCK;

	Copyright (C) 2012 Victor Soria-Carrasco
	Department of Animal and Plant Sciences
	University of Sheffield
	Sheffield S10 2ST
	United Kingdom
	v.soria-carrasco\@sheffield.ac.uk

BLOCK
	print "$auth";
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub usage{
	author();
	print "\tbfca_tool1_2by2.pl -i <xml file> -c <calibrations/constraints file> -o <output directory>\n\n";
	exit();
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_cmd_line{
	my @cmd=(@_);
	my $xml;
	my $constraints;
	my $outdir;
	my $basename;

	my $op;
	foreach (@ARGV){
		if (/-i/){ $op="xml"; next; }
		elsif (/-c/){ $op="constraints"; next; }
		elsif (/-o/){ $op="outdir"; next; }

		if ($op eq "xml"){ $xml = $_; }
		elsif($op eq "constraints"){ $constraints = $_; }
		elsif($op eq "outdir"){ $outdir = $_; }
	}
	if (defined ($xml)){
		$basename=$xml;
		$basename=~ s/.+[\/|\\]//g;
		$basename=~ s/\..+//g;
		if  (!defined($outdir)){
			$outdir=$xml;
			$outdir=~ s/$basename.*//g;
			$outdir='.' if ($outdir eq "");
		}
	}

	return ($xml, $constraints, $outdir, $basename);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_taxablock{
	# DEFINE TAXA BLOCK
	my @calibrations=@_;

	my $taxablock="";
	foreach my $cal (@calibrations){
		$taxablock.='	<taxa id="'.${$cal}[0].'">'."\n";
		my @taxa=split(/\,/,${$cal}[1]);
		foreach my $t (@taxa){
			$taxablock.='		<taxon idref="'.$t.'"/>'."\n";
		}
		$taxablock.='	</taxa>'."\n";
	}
	return($taxablock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub clock_estimation{
	my @xml=@_;

	my @cl=();
	foreach (@xml){
		if (/parameter.+clock\.rat.+value/ || /parameter.+ucld\.mean.+value/ || /parameter.+uced\.mean.+value/){				
			my $aux=$_;
			chomp($aux);
			$aux=~ s/.+id\=\"//g;
			$aux=~ s/\".+//g;
			push (@cl, $aux) if (!grep(/^$aux$/, @cl));
		}
	}

	my @newxml=();
	my $jump=0;

	foreach (@xml){
		if ($jump > 0){
			$jump--;
			next;
		}
		
		# Change prior
		if (/\<parameter id\=\".*clock\.rate\"/){
			s/clock\.rate.+/clock\.rate\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		if (/\<parameter id\=\".*ucld\.mean\"/){
			s/ucld\.mean.+/ucld\.mean\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		if (/\<parameter id\=\".*uced\.mean\"/){
			s/uced\.mean.+/uced\.mean\" value\=\"0\.5\" lower\=\"0\.0\" upper\=\"100\.0\"\/\>/g if (!/lower/);
		}
		
		push (@newxml, $_);
		
		# Add scaleOperator
		
		if (/\<operators id\=/){
			foreach my $c (@cl){
				push (@newxml,
					"\t\t".'<scaleOperator scaleFactor="0.75" weight="3">'."\n");
				push (@newxml,
					"\t\t\t".'<parameter idref="'.$c.'"/>'."\n");
				push (@newxml,
					"\t\t".'</scaleOperator>'."\n");
			}
		}

		# Add upDownOperator
		if (/<up>/){
			my $c=shift(@cl);
			push (@newxml,
				"\t\t\t\t".'<parameter idref="'.$c.'"/>'."\n");
		}
	}
	
	return(@newxml);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_tmrcastatblock{
	#DEFINE TMRCA STATS BLOCK
	my @calibrations=@_;

	my $tmrcastatblock="";
	foreach my $cal (@calibrations){
		$tmrcastatblock.="\t".'<tmrcaStatistic id="tmrca('.${$cal}[0].')">'."\n";
		$tmrcastatblock.="\t\t".'<mrca>'."\n";
		$tmrcastatblock.="\t\t\t".'<taxa idref="'.${$cal}[0].'"/>'."\n";
		$tmrcastatblock.="\t\t".'</mrca>'."\n";
		$tmrcastatblock.="\t\t".'<treeModel idref="treeModel"/>'."\n";
		$tmrcastatblock.="\t".'</tmrcaStatistic>'."\n";
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$tmrcastatblock.="\t".'<monophylyStatistic id="monophyly('.${$cal}[0].')">'."\n";
			$tmrcastatblock.="\t\t".'<mrca>'."\n";
			$tmrcastatblock.="\t\t\t".'<taxa idref="'.${$cal}[0].'"/>'."\n";
			$tmrcastatblock.="\t\t".'</mrca>'."\n";
			$tmrcastatblock.="\t\t".'<treeModel idref="treeModel"/>'."\n";
			$tmrcastatblock.="\t".'</monophylyStatistic>'."\n";
		}
	}

	return ($tmrcastatblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_treeblock{
	# DEFINE TREE BLOCK
	my @calibrations=@_;

	my $treeblock='	<!-- Generate a random starting tree under the coalescent process            -->'."\n";
	$treeblock.='	<!-- Has calibration                                                         -->'."\n";
	$treeblock.='	<coalescentTree id="startingTree">'."\n";
	$treeblock.='		<constrainedTaxa>'."\n";
	$treeblock.='			<taxa idref="taxa"/>'."\n";

	foreach my $cal (@calibrations){
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$treeblock.='			<tmrca monophyletic="true">'."\n";
		}
		else{
			$treeblock.='			<tmrca monophyletic="false">'."\n";
		}
		$treeblock.='				<taxa idref="'.${$cal}[0].'"/>'."\n";
		if (${$cal}[3] eq "truncated-normal"){
			# Distribution block
			$treeblock.='				<uniformDistributionModel>'."\n";
			$treeblock.='					<lower>'.sprintf("%.1f",${$cal}[6]).'</lower>'."\n";
			$treeblock.='					<upper>'.sprintf("%.1f",${$cal}[7]).'</upper>'."\n";
			$treeblock.='				</uniformDistributionModel>'."\n";
		}
		elsif(${$cal}[3] eq "uniform"){
			# Distribution block
			$treeblock.='				<uniformDistributionModel>'."\n";
			$treeblock.='					<lower>'.sprintf("%.1f",${$cal}[4]).'</lower>'."\n";
			$treeblock.='					<upper>'.sprintf("%.1f",${$cal}[5]).'</upper>'."\n";
			$treeblock.='				</uniformDistributionModel>'."\n";
		}
		$treeblock.='			</tmrca>'."\n";
	}

	$treeblock.='		</constrainedTaxa>'."\n";
	$treeblock.='		<constantSize idref="initialDemo"/>'."\n";
	$treeblock.='	</coalescentTree>'."\n";

	return($treeblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_priorblock{
	#DEFINE PRIOR BLOCK
	my @calibrations=@_;

	my $priorblock="\t\t\t".'<prior id="prior">'."\n";
	$st=0;
	my $monophylies="\t\t\t\t".'<booleanLikelihood>'."\n";
	foreach my $cal (@calibrations){
		if (${$cal}[2] eq "yes"){# enforce monophyly
			$monophylies.="\t\t\t\t\t".'<monophylyStatistic idref="monophyly('.${$cal}[0].')"/>'."\n";
			$st=1;
		}
	}
	$monophylies.="\t\t\t\t".'</booleanLikelihood>'."\n";
	$priorblock.=$monophylies if ($st==1);

	foreach my $cal (@calibrations){
		if (${$cal}[3] eq "lognormal"){
			$priorblock.="\t\t\t\t".'<logNormalPrior mean="'.sprintf("%.1f",${$cal}[4]).'" stdev="'.sprintf("%.1f",${$cal}[5]).'" offset="'.sprintf("%.1f",${$cal}[6]).'" meanInRealSpace="true">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</logNormalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "normal"){
			$priorblock.="\t\t\t\t".'<normalPrior mean="'.sprintf("%.1f",${$cal}[4]).'" stdev="'.sprintf("%.1f",${$cal}[5]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</normalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "exponential"){
			$priorblock.="\t\t\t\t".'<exponentialPrior mean="'.sprintf("%.1f",${$cal}[4]).'" offset="'.sprintf("%.1f",${$cal}[5]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</exponentialPrior>'."\n";
		}
		elsif (${$cal}[3] eq "truncated-normal"){
			$priorblock.="\t\t\t\t".'<uniformPrior lower="'.sprintf("%.1f",${$cal}[6]).'" upper="'.sprintf("%.1f",${$cal}[7]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</uniformPrior>'."\n";
			$priorblock.="\t\t\t\t".'<normalPrior mean="'.sprintf("%.1f",${$cal}[4]).'" stdev="'.sprintf("%.1f",${$cal}[5]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</normalPrior>'."\n";
		}
		elsif (${$cal}[3] eq "gamma"){
			$priorblock.="\t\t\t\t".'<gammaPrior shape="'.sprintf("%.1f",${$cal}[4]).'" scale="'.sprintf("%.1f",${$cal}[5]).'" offset="'.sprintf("%.1f",${$cal}[6]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</gammaPrior>'."\n";
		}
		elsif (${$cal}[3] eq "invgamma"){
			$priorblock.="\t\t\t\t".'<invgammaPrior shape="'.sprintf("%.1f",${$cal}[4]).'" scale="'.sprintf("%.1f",${$cal}[5]).'" offset="'.sprintf("%.1f",${$cal}[6]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</invgammaPrior>'."\n";
		}
		elsif (${$cal}[3] eq "uniform"){
			$priorblock.="\t\t\t\t".'<uniformPrior lower="'.sprintf("%.1f",${$cal}[4]).'" upper="'.sprintf("%.1f",${$cal}[5]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</uniformPrior>'."\n";
		}
		elsif (${$cal}[3] eq "laplace"){
			$priorblock.="\t\t\t\t".'<laplacePrior mean="'.sprintf("%.1f",${$cal}[4]).'" scale="'.sprintf("%.1f",${$cal}[5]).'">'."\n";
			$priorblock.="\t\t\t\t\t".'<statistic idref="tmrca('.${$cal}[0].')"/>'."\n";
			$priorblock.="\t\t\t\t".'</laplacePrior>'."\n";
		}
	}

	return($priorblock);
}

#-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-:-

sub get_logblock{
	# DEFINE LOG BLOCK
	my @calibrations=@_;

	my $logblock="";
	foreach my $cal (@calibrations){
		$logblock.="\t\t\t".'<tmrcaStatistic idref="tmrca('.${$cal}[0].')"/>'."\n";
	}

	return($logblock);
}
