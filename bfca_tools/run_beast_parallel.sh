#!/bin/bash

# Copyright (C) 2012 Victor Soria-Carrasco
# Department of Animal and Plant Sciences
# University of Sheffield
# Western Bank 
# Sheffield S10 2TN
# United Kingdom
# v.soria-carrasco@sheffield.ac.uk

# Previous address:
# Institute of Evolutionary Biology (IBE), CSIC-UPF
# Pg. Maritim de la Barceloneta 37-42, 08003 Barcelona, Spain
# victor.soria@ibe.upf-csic.es

# This program is part of the bfca tools

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# Given directories with alignments and trees generates topological constrained
# xml to be run on beast



# Given a directory containing XML BEAST files, it runs BEAST analysis on every one using all
# available cpus (and gpus) in the computer

# run_beast_parallel <directory with xmls>


# ==================================================================================================
# Set path to BEAST
BEAST='beast'

# Set to 1 if you want to use your GPU for computations 
# (requires BEAGLE library, a NVIDIA CUDA capable graphics card, and CUDA drivers and toolkit)
USE_GPU=0 

# Set to 1 if you want to use SSE instructions for faster computation 
# (requires BEAGLE library and cpu with SSE instructions)
USE_SSE=0 

# Specify number of CPUs of GPUs to be used (optional)
NCPU=0
NGPU=0

WAITTIME=30
# ==================================================================================================

BEAST_GPU="$BEAST -beagle -beagle_GPU -overwrite"
if [ "$USE_SSE" -eq "1" ];
then
	BEAST_CPU="$BEAST -beagle -beagle_SSE -overwrite"
else
	BEAST_CPU="$BEAST -overwrite"
fi


# BEAST_GPU="$BEAST -beagle -beagle_GPU -overwrite -working"
# if [ "$USE_SSE" -eq "1" ];
# then
# 	BEAST_CPU="$BEAST -beagle -beagle_SSE -overwrite -working"
# else
# 	BEAST_CPU="$BEAST -overwrite -working"
# fi

cd $1
# XMLS=`find . -maxdepth 1 -name "*.xml"`
XMLS=`find . -maxdepth 2 -name "*.xml"`

# Get OS, # cpus, and gpu
# --------------------------------------------------------------------------------------------------
OS=`uname`

if [ $NCPU -eq 0 ];
	then
	if [ "$OS" == "Darwin" ];
	then
		NCPU=`system_profiler SPHardwareDataType | grep "Number Of Cores" | perl -pi -e 's/.+\: //g; chomp($_);'`
	elif [ "$OS" == "Linux" ];
	then
		NCPU=`grep -ch "cpu cores" /proc/cpuinfo | perl -pi -e 'chomp($_)'`
	fi
fi

if [[ $USE_GPU -eq 1 && $NGPU -eq 0 ]];
then
	if [ "$OS" == "Darwin" ];
	then
		NGPU=`system_profiler | grep -ch "^    NVIDIA GeForce"`
	elif [ "$OS" == "Linux" ];
	then
		NGPU=`lspci | grep -i "nvidia" | grep -chi "VGA"`
	fi
fi

echo
echo "$OS, CPU: $NCPU, GPU: $NGPU"

if [ "$NGPU" -eq "0" ] && [ "$USE_GPU" -eq "1" ];
then
	echo "ERROR: NVIDIA GPU NOT FOUND"
	exit
fi

if [ "$NGPU" -lt "0" ] && [ "$USE_GPU" -eq "1" ];
then
	NCPU=$(expr $NCPU - 1)
fi

if [ "$USE_GPU" -eq "1" ];
then
	echo "NVIDIA GPU will be used"
else
	echo "NVIDIA GPU will not be used"
fi;

# --------------------------------------------------------------------------------------------------

JOBS=""
c=1
for xml in $XMLS;
do
	JOBS[$c]=$xml
	c=$(expr $c + 1)	
done

let NJOBS=${#JOBS[*]}-1

echo "$NJOBS jobs:"

# Check jobs running on GPU and CPU
NGPUJOBS=`ps u | grep -v "grep" | grep -chE "java.+beagle_GPU" | perl -pi -e 'chomp($_);'`
NCPUJOBS=`ps u | grep -v "grep" | grep -v "beagle_GPU" | grep -chE "java.+beast" | perl -pi -e 'chomp($_);'`

c=1
nw=0
DONE=0
while [ "$DONE" -eq "0" ];
do

# 	echo "	CPU jobs running: $NCPUJOBS"

	# Launch jobs
	# ----------------------------------------------------------------------------------------------
	if [ "$USE_GPU" -eq "1" ];
	then
		echo "	GPU jobs running: $NGPUJOBS"

		if [[ "$NGPUJOBS" -lt "$NGPU" && "$c" -lt "${#JOBS[*]}" ]];
		then
			# Send job to GPU
			echo "	Job ${JOBS[$c]} now running on GPU"
			GO="1"
			while [ "$GO" -eq "1" ];
			do
				$BEAST_GPU ${JOBS[$c]} >& ${JOBS[$c]}.log.txt &
				sleep 5
				GO=`grep -ch "initial posterior is zero" ${JOBS[$c]}.log.txt`
			done
			c=$(expr $c + 1)
		elif [[ "$NCPUJOBS" -lt "$NCPU" && "$c" -lt "${#JOBS[*]}" ]]
		then
			echo "	Job ${JOBS[$c]} now running on CPU"
			GO="1"
			while [ "$GO" -eq "1" ];
			do
				$BEAST_CPU ${JOBS[$c]} >& ${JOBS[$c]}.log.txt &
				sleep 5
				GO=`grep -ch "initial posterior is zero" ${JOBS[$c]}.log.txt`
			done
			c=$(expr $c + 1)
		fi
	else
		if [[ "$NCPUJOBS" -lt "$NCPU" && "$c" -lt "${#JOBS[*]}" ]]
		then
			echo "	Job ${JOBS[$c]} now running on CPU"
			GO="1"
			while [ "$GO" -eq "1" ];
			do
				$BEAST_CPU ${JOBS[$c]} >& ${JOBS[$c]}.log.txt &
				sleep 5
				GO=`grep -ch "initial posterior is zero" ${JOBS[$c]}.log.txt`
			done
			c=$(expr $c + 1)
		fi	
	fi
	# ----------------------------------------------------------------------------------------------


	NGPUJOBS=`ps u | grep -v "grep" | grep -chE "java.+beagle_GPU" | perl -pi -e 'chomp($_);'`
	NCPUJOBS=`ps u | grep -v "grep" | grep -v "beagle_GPU" | grep -chE "java.+beast" | perl -pi -e 'chomp($_);'`

	if [[ "$USE_GPU" -eq "1" && "$NGPUJOBS" -eq "$NGPU" && "$NCPUJOBS" -eq "$NCPU" ]] ||\
	   [[ "$USE_GPU" -eq "0" && "$NCPUJOBS" -eq "$NCPU" ]];
	then
		if [ "$nw" -eq "1" ];
		then
		 	echo "	CPU jobs running: $NCPUJOBS"
	 		if [ "$USE_GPU" -eq "1" ];
			then
				echo "	GPU jobs running: $NGPUJOBS"
			fi
			echo
			echo "	Waiting (1 point = $WAITTIME secs)"
			echo -n "	."
		else
			chk=`echo $nw | perl -pi -e '$_=int($_%80)'`
			if [ "$chk" -eq "0" ];
			then
				echo
				echo -n "	."
			else
				echo -n "."
			fi		
		fi
		sleep $WAITTIME
	fi

	# Check if jobs have finished
	# ----------------------------------------------------------------------------------------------		
	if [[ "$USE_GPU" -eq "1" && "$c" -eq "${#JOBS[@]}" && "$NGPUJOBS" -eq "0" && "$NCPUJOBS" -eq "0" ]] ||\
	   [[ "$USE_GPU" -eq "0" && "$c" -eq "${#JOBS[@]}" && "$NCPUJOBS" -eq "0" ]];
	then
		DONE=1
	fi
	# ----------------------------------------------------------------------------------------------			
	let nw=nw+1
done
